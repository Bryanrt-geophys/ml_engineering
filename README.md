# ML_Engineering

This project is a compilation of work done to demonstrate ability to perform daily tasks in ML engineering.


The project can be found [here](https://gitlab.com/Bryanrt-geophys/ml_engineering/-/blob/main/Practical%20Exercise%20for%20SCG%20LLC%20Interview/Bryan_Thomas_solution-2.ipynb).

## Project Overview

This project required gathering images from an Amazon Web Services (AWS) cloud server, creating a pipeline to batch and process the images before passing them to two deep learning models of my choice, pretrained on the ImageNet data set.

I chose to work with the VGG16 and the ResNet50 models to get a reference on the difference that skip-connections can make in run-time and accuracy of a convolution neural network.

## Comments on the Experience

Despite my primary coding experience being in the R programming language, this demonstrates my ability to quickly learn industry standard tools such as AWS, Python, and Tensorflow. Though my code may not be the most profound use of Python, it demonstrates an understanding of fundamental principles in workflows and deep learning. Ideally, I would have leveraged concepts of vectorization to be more efficient than using certain loops but that is something I still have to master in Python and, given the time limit I had on the exercise, I determined this wasn't the opportunity to explore that.

Solution Consulting Group, LLC was a great group to work with during this exercise. I found the interview exercise to be a fantastic way to see myself in the role and get to know what it would be like to collaborate with the team. They were highly professional and encouraging. After the experience they offered  [two solutions](https://gitlab.com/Bryanrt-geophys/ml_engineering/-/tree/main/Practical%20Exercise%20for%20SCG%20LLC%20Interview/Solution_Reference's) examples for my reference to see how to execute some of the ideas I expressed I believed would be important but did not have the time to learn how to code in a new language. 
